//Elise Canouse
//ecanous
//14 February 2019
//CPSC 1021 - 003
//Lab 5
//Nushrat Humaira



#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"


using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };


//creates a struct to hold the cards
typedef struct Card {
	Suit suit;
	int value;
} Card;

//function prototypes
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {

//creates a new seed each time the program is run
	srand(unsigned (time(0)));


//creates an array of card structs
	Card deck[52];
//initializes each card in the array to a suit and a value 2-14
	for(int i = 0; i < 52; i++){
		int numCard = i % 13;
		deck[i].value = numCard+2;
		if(i < 13){
			deck[i].suit = SPADES;
		} else if (i < 26){
			deck[i].suit = HEARTS;
		} else if (i < 39){
			deck[i].suit = DIAMONDS;
		} else {
			deck[i].suit = CLUBS;
		}
	}



  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
//randomly shuffles deck of cards
	random_shuffle(&deck[0], &deck[52], myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
//creates a new array of structs and initializes it to the first 5 cards in the deck struct
	Card hand[5];
	for(int i = 0; i < 5; i++){
		hand[i] = deck[i];
	}


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
//sorts the hand first by suit and then by value
	sort(&hand[0], &hand[5], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

//prints each of the cards in the hand
	string code;
	string name; 
	for(int i = 0; i < 5; i++){
		code = get_suit_code(hand[i]);
		if(hand[i].value > 10){
			name = get_card_name(hand[i]);
			cout << setw(10) << right << name << " of " << code << endl;
		}else{
			cout << setw(10) << right << hand[i].value << " of " << code << endl;
		}
	}

	return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

//compares two cards and returns true if the card on the left is less than the card on the right, otherwise returns false
bool suit_order(const Card& lhs, const Card& rhs) {

	if(lhs.suit < rhs.suit){
		return true;
	}else if(lhs.suit == rhs.suit){
		if(lhs.value < rhs.value){
			return true;
		}
	}
	return false;

}

//retrieves code for printing suit characters
string get_suit_code(Card& c) {
	switch (c.suit) {
		case SPADES:    return "\u2660";
		case HEARTS:    return "\u2661";
		case DIAMONDS:  return "\u2662";
		case CLUBS:     return "\u2663";
		default:        return "";
	}
}

//if face card, returns name rather than number
string get_card_name(Card& c) {
	// IMPLEMENT
	//when given a number, return a string
	if(c.value == 11){
		return "Jack";
	}else if(c.value == 12){
		return "Queen";
	}else if(c.value == 13){
		return "King";
	}else if(c.value == 14){
		return "Ace";
	}else{
		return "";
	}
	
}
